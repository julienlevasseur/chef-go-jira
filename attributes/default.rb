default['go-jira']['version']  = '1.0.20'
default['go-jira']['platform'] = 'amd64'

default['go-jira']['install_dir']  = '/usr/local/bin'
default['go-jira']['install_name'] = 'jira' # The name of the binary