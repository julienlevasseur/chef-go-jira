#
# Cookbook:: chef-go-jira
# Recipe:: install
#
# Copyright:: 2018, Julien Levasseur, All Rights Reserved.

gojira node['go-jira']['version'] do
  action :install
  platform     node['go-jira']['platform']
  install_dir  node['go-jira']['install_dir']
  install_name node['go-jira']['install_name']
end
