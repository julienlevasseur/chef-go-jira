#
# Chef Documentation
# https://docs.chef.io/libraries.html
#

class Chef
  class Recipe
    module GoJira
      def self.is_installed?(install_dir, install_name)
        return File.file?("#{install_dir}/#{install_name}")
      end

      def self.version(install_dir, install_name)
        require 'open3'
        if self.is_installed?(install_dir, install_name)
          cmd = "#{install_dir}/#{install_name} version"
          _stdin, stdout, _stderr = Open3.popen3(cmd)
          cmd_out = stdout.readlines
          return cmd_out[0].chop
        else
          return '0.0.0'
        end
      end

      def self.download_release(install_dir, install_name, version, platform)
        require 'open-uri'
        File.open("#{install_dir}/#{install_name}", "wb") do |file|
          file.write open("https://github.com/Netflix-Skunkworks/go-jira/releases/download/v#{version}/jira-linux-#{platform}").read          
        end
        FileUtils.chmod 0755, "#{install_dir}/#{install_name}"
      end

      def uninstall(install_dir, install_name)
        File.delete("#{install_dir}/#{install_name}")
      end
    end
  end
end
