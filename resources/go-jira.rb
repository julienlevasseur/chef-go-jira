resource_name :gojira
provides :gojira

property :version, String, name_property: true
property :platform, String
property :install_dir, String
property :install_name, String

default_action :install

action :install do
  unless ::Chef::Recipe::GoJira.is_installed?(new_resource.install_dir, new_resource.install_name) ||
  ::Chef::Recipe::GoJira.version(new_resource.install_dir, new_resource.install_name) == new_resource.version
    converge_by "Installing Go-Jira #{new_resource.version}" do
      begin
        ::Chef::Recipe::GoJira.download_release(new_resource.install_dir, new_resource.install_name, new_resource.version, new_resource.platform)
      rescue
        Chef::Log.error("Failed to install Go-Jira #{new_resource.version}")
      end
    end
  end
end

action :uninstall do
  if ::Chef::Recipe::GoJira.is_installed?(new_resource.install_dir, new_resource.install_name)
    converge_by "Uninstalling Go-Jira #{new_resource.version}" do
      begin
        ::Chef::Recipe::GoJira.uninstall(new_resource.install_dir, new_resource.install_name)
      rescue
        Chef::Log.error("Failed to uninstall Go-Jira #{new_resource.version}")
      end
    end
  end
end
